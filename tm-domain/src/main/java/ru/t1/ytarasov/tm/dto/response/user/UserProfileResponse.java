package ru.t1.ytarasov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.response.AbstractResponse;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserProfileResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public UserProfileResponse(@Nullable UserDTO user) {
        this.user = user;
    }

}
