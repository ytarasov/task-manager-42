package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractModel {

    @Column
    @Nullable
    private String login;

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(name = "locked")
    private boolean isLocked = false;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions;

}
