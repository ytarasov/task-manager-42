package ru.t1.ytarasov.tm.api.service;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @Nullable
    List<ProjectDTO> findAll();

    @Nullable
    List<ProjectDTO> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable final Sort sort) throws Exception;

    int getSize() throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws Exception;

    @Nullable
    ProjectDTO findOneById(@Nullable final String id) throws Exception;

    @Nullable
    ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    @Nullable
    ProjectDTO remove(@Nullable final ProjectDTO project) throws Exception;

    @Nullable
    ProjectDTO removeById(@Param("id") @Nullable final String id) throws Exception;

    @Nullable
    ProjectDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO project) throws Exception;

    ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception;

    void clear() throws Exception;

    void clear(@Nullable final String userId) throws Exception;

}